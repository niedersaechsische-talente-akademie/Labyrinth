package io.noim.daslabyrinth;

/**
 * Created by nilsbergmann on 28.07.16.
 */
public enum Richtung {

    Oben, Unten, Rechts, Links
}
